var express = require('express'); //Charge Express
var session = require('cookie-session'); // Charge le middleware de sessions
var bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
var urlencodedParser = bodyParser.urlencoded({
  extended: false
});




  //  Quand un client se connecte, il récupère la dernière Todolist connue du serveur                   //REALISE
  //  Quand un client ajoute une tâche, celle-ci est immédiatement répercutée chez les autres clients   //REALISE
  //  Quand un client supprime une tâche, celle-ci est immédiatement supprimée chez les autres clients  //REALISE


//on demarre l'application
var app = express();
//On declare le tableau (nous n'avons besoin ni de sessions ni de cookies car partagé)
var arrayTaches = [];
//Autre middleware interessant
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var ent = require('ent');


//On utilise Express pour une redirection efficace
app.get('/todo', function(req, res) {
  res.render('view.ejs', {
    todo: arrayTaches
  });
});

//On declare l'evement connection d'un utilisateur
io.sockets.on('connection', function(socket) {
  //Dès que l'utilisateur se connecte, on envoie le tableau de taches
  socket.emit('change', arrayTaches);

  //Evenement qui ajoute une tache au tableau
  socket.on('ajouterTache', function(nouvelleTache) {
    arrayTaches.push(nouvelleTache);
    socket.broadcast.emit('change', arrayTaches);
    socket.emit('change', arrayTaches);
  });

  //evenement qui supprime une tache au tableau
  socket.on('supprimerTache', function(idTache){
    arrayTaches.splice(parseInt(idTache), 1);
    socket.broadcast.emit('change', arrayTaches);
    socket.emit('change', arrayTaches);
  });

  //evenement qui modifie une tache au tableau
  socket.on('modifierTache', function(modifTache){
    arrayTaches[parseInt(modifTache.id)] = modifTache.valeur;
    socket.broadcast.emit('change', arrayTaches);
    socket.emit('change', arrayTaches);
  });

});

//on redirige les urls inconnus
app.use(function(req, res, next) {
  res.setHeader('Content-Type', 'text/plain');
  res.status(404).send('Page introuvable !');
});

//on ecoute sur le port 8080 (on utilise serveur et non app car sinon il est impossible de charger IO coté client)
server.listen(8080);
