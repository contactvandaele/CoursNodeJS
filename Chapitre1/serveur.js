var http = require('http');
var url = require("url");
var querystring = require('querystring');
var EventEmitter = require('events').EventEmitter;


var instructionsNouveauVisiteur = function(req, res) {
	var page = url.parse(req.url).pathname;
	var params = querystring.parse(url.parse(req.url).query);
    console.log(page);
    res.writeHead(200, {"Content-Type": "text/plain"});
     if ('prenom' in params && 'nom' in params) {
        res.write('Vous vous appelez ' + params['prenom'] + ' ' + params['nom']);
    }
    else {
        res.write('Vous devez bien avoir un prénom et un nom, non ?');
    }
    res.end();
}

var server = http.createServer(instructionsNouveauVisiteur);

var jeu = new EventEmitter();

jeu.on('gameover', function(message){
    console.log(message);
});

jeu.on('nouveaujoueur', function(nom, age){
	console.log("Le joueur " + nom + " vient d'arriver, il a " + age + " ans.")
})

//jeu.emit('gameover', 'Vous avez perdu !');
//jeu.emit('nouveaujoueur', 'Mario', 35); // Envoie le nom d'un nouveau joueur qui arrive et son âge

var markdown = require('markdown').markdown;
console.log(markdown.toHTML('Un paragraphe en **markdown** !'));


server.listen(8080);
