var express = require('express');
var session = require('cookie-session'); // Charge le middleware de sessions
var bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
var urlencodedParser = bodyParser.urlencoded({
  extended: false
});

//on demarre l'application
var app = express();
//On declare 2 parseur, l'un accepte les retours HTML l'autre les données Json
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = app.use(bodyParser.json());

/* On utilise les sessions */
app.use(session({
  secret: 'todotopsecret'
}));


app.use(function(req, res, next) {
  if (typeof(req.session.todolist) == 'undefined') {
    req.session.todolist = [];
  }
  next();
});

app.get('/todo', function(req, res) {
  res.render('view.ejs', {
    todo: req.session.todolist
  });
});

app.post('/todo/ajouter',urlencodedParser, function(req, res) {
  req.session.todolist.push(req.body.a_ajouter);
  res.redirect("/todo");
});

app.post('/todo/modifier',urlencodedParser, function(req, res) {
  console.log(req.body.old_value);
  req.session.todolist[parseInt(req.body.old_value)] = req.body.new_value ;
  console.log(req.session.todolist);
  res.redirect("/todo");
});

app.get('/todo/supprimer/:id', function(req, res) {
  console.log(req.params.id);
  req.session.todolist.splice(req.params.id, 1);
  res.redirect("/todo");
});
app.use(function(req, res, next) {
  res.setHeader('Content-Type', 'text/plain');
  res.status(404).send('Page introuvable !');
});

app.listen(8080);
